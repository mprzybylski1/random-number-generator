# Random Number Generator

## About
This is a simple Java application based on Java 11, which pseudo randomly generates two sets of numbers. The first set contains integers, and the other one contains floats, which represent probabilities from 0 to 1 at which the integers from the first set can be randomly selected. All generated probabilities sum up to 1.

The application requires three arguments to be provided by the user through the command line when starting up the application.

```
-bound: This is an upper bound of the number range, from which numbers will be generated at random
-amount_needed: The amount of numbers to be generated from the range
-reps: Amount of repetitions nextNum method is called
```

# How To Use
Clone GIT repository to your directory of choice and go to target directory. This directory contains already build .jar file which is run by the following command:
```
java -jar random-number-generator-1.0-SNAPSHOT-jar-with-dependencies.jar -bound <bound_arg> -amount_needed <amount_needed_arg> -reps <reps_arg>

#Example: Running the application to generate 5 random integers from 0 to 9 and selecting them 1000000 times using generated probabilities
java -jar random-number-generator-1.0-SNAPSHOT-jar-with-dependencies.jar -bound 10 -amount_needed 5 -reps 1000000
```

# SQL Question
An answer to the SQL question is provided at the end of the [postgreSQL.sql file](/SQL/postgreSQL.sql) <-- Please click here.
