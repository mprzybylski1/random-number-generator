--SQL for tables and sample data from the question

create table product
(
    product_id integer primary key,
    name character varying(128) not null,
    rrp integer not null,
    available_from date not null
);

insert into product VALUES (101, 'Bayesian Methods for Nonlinear Classification and Regression', 94.95, '2022-04-01');
insert into product values (102, '(next year) in Review (preorder)', 21.95, '2023-04-08');
insert into product values (103, 'Learn Python in Ten Minutes', 2.15, '2022-01-08');
insert into product values (104, 'sports almanac (1999-2049)', 3.38, '2020-04-01');
insert into product values (105, 'finance for dummies', 84.99, '2021-04-01');

create table orders
(
    order_id integer primary key,
    product_id integer not null,
    quantity integer not null,
    order_price integer not null,
    dispatch_date date not null,
    foreign key (product_id) references product(product_id)
);

insert into orders values (1000, 101, 1, 90, '2022-02-08');
insert into orders values (1001, 103, 1, 1.15, '2022-02-27');
insert into orders values (1002, 101, 10, 90, '2021-05-08');
insert into orders values (1003, 104, 11, 3.38, '2021-10-08');
insert into orders values (1004, 105, 11, 501.33, '2020-04-08');

--********************ANSWER********************--
--SQL query to fetch all books that are available for more than a month, and have sold fewer than 10 copies in the last year
SELECT p.* FROM product p join orders o on p.product_id = o.product_id where o.quantity < 10 and o.dispatch_date >= date_trunc('day', now() - INTERVAL '1 year') and o.dispatch_date < date_trunc('day', now()) and p.available_from < date_trunc('day', now() - INTERVAL '1 month')