package demo.generator;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class RandomGenTests {

    private static final int topBound = 10;
    private static final int amountNeeded = 5;

    @Test
    void givenBoundBeingZero_whenValidatingArguments_thenThrowIllegalArgumentException() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new RandomGen(0, 1));
        assertTrue(exception.getMessage().contains("topNumberBound must be greater than 0"));
    }

    @Test
    void givenAmountOfNumbersBeingZero_whenValidatingArguments_thenThrowIllegalArgumentException() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new RandomGen(1, 0));
        assertTrue(exception.getMessage().contains("amountOfNumbersNeeded must be greater than 0"));
    }

    @Test
    void givenBoundBeingLessThanAmountOfNumbers_whenValidatingArguments_thenThrowIllegalArgumentException() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new RandomGen(1, 2));
        assertTrue(exception.getMessage().contains("topNumberBound must be larger than amountOfNumbersNeeded"));
    }

    @Test
    public void givenStandardInitialisation_whenNumbersGenerated_thenAllElementsShouldBeGreaterThanOrEqualToZero_andLessThanTopBound(){
        final RandomGen generator = new RandomGen(topBound, amountNeeded);
        final Integer[] numbers = generator.generateRandomNumbers();

        assertEquals(amountNeeded, numbers.length);
        assertTrue(Stream.of(numbers).allMatch(n -> n >= 0));
        assertTrue(Stream.of(numbers).allMatch(n -> n < topBound));
    }

    @Test
    public void givenStandardInitialisation_whenProbabilitiesGenerated_thenShouldAddUpToOne(){
        final float expectedSum = 1f;

        final RandomGen generator = new RandomGen(topBound, amountNeeded);
        final Float[] probabilities = generator.generateProbabilities();

        assertEquals(amountNeeded, probabilities.length);

        final float addedProbabilities = Stream.of(probabilities).reduce(0f, Float::sum);
        assertEquals(expectedSum, addedProbabilities, 0.0000001);
    }

    @Test
    public void givenStandardInitialisation_whenCallingNextNumWithIllegalRepetitionsArgument_thenThrowIllegalArgumentException(){
        final int repetitions = 0;
        final RandomGen generator = new RandomGen(topBound, amountNeeded);

        final Exception exception = assertThrows(IllegalArgumentException.class, () -> generator.nextNum(repetitions));
        assertTrue(exception.getMessage().contains("repetitions must be greater than 0"));
    }

    @Test
    void givenStandardInitialisation_whenCallingNextNumMillionTimes_thenNumbersShouldBeChosenAtGeneratedProbabilities() {
        final int repetitions = 1_000_000;
        final RandomGen generator = new RandomGen(topBound, amountNeeded, false);

        final Integer[] randomNumbers = generator.generateRandomNumbers();
        final Float[] probabilities = generator.generateProbabilities();
        System.out.println(generator.nextNum(repetitions));

        final Map<Integer, Integer> count = generator.getCounter();

        for(int i =0; i<randomNumbers.length; i++){
            assertEquals(probabilities[i], round((float)count.get(randomNumbers[i]) / repetitions, 2), 0.02);
        }
    }

    private float round(float number, int scale) {
        return new BigDecimal(number).setScale(scale, RoundingMode.HALF_EVEN).floatValue();
    }

}
