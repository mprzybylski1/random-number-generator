package demo.generator;

import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class RandomGen {

    private static final Logger LOG = LoggerFactory.getLogger(RandomGen.class);

    private final Random randomNumGen;
    private final int topNumberBound;
    private final int amountOfNumbersNeeded;
    private final Map<Integer, Integer> counter = new HashMap<>();

    private Integer[] randomNums;
    private Float[] probabilities;

    /**
    * Creates a new instance of RandomGen with a new instance of {@link Random} and autoInitialise argument set to true.
    * */
    public RandomGen(final int topNumberBound, final int amountOfNumbersNeeded){
        this(new Random(), topNumberBound, amountOfNumbersNeeded, true);
    }

    /**
     * Creates a new instance of RandomGen with a new instance of {@link Random}.
     * */
    public RandomGen(final int topNumberBound, final int amountOfNumbersNeeded, final boolean autoInitialise){
        this(new Random(), topNumberBound, amountOfNumbersNeeded, autoInitialise);
    }

    public RandomGen(final Random randomNumGen, final int topNumberBound, final int amountOfNumbersNeeded, final boolean autoInitialise){
        validateArguments(topNumberBound, amountOfNumbersNeeded);
        this.randomNumGen = randomNumGen;
        this.topNumberBound = topNumberBound;
        this.amountOfNumbersNeeded = amountOfNumbersNeeded;

        if(autoInitialise)
            initialise();
    }

    private void validateArguments(final int topNumberBound, final int amountOfNumbersNeeded){
        LOG.debug("validateArguments : topNumberBound=[{}], amountOfNumbersNeeded=[{}]", topNumberBound, amountOfNumbersNeeded);

        if(topNumberBound < 1)
            throw new IllegalArgumentException("topNumberBound must be greater than 0");
        if(amountOfNumbersNeeded < 1)
            throw new IllegalArgumentException("amountOfNumbersNeeded must be greater than 0");
        if(amountOfNumbersNeeded > topNumberBound)
            throw new IllegalArgumentException("topNumberBound must be larger than amountOfNumbersNeeded");
    }

    private void initialise(){
        LOG.debug("initialise : topNumberBound=[{}], amountOfNumbersNeeded=[{}]", topNumberBound, amountOfNumbersNeeded);

        generateRandomNumbers();
        generateProbabilities();
    }

    public Integer[] generateRandomNumbers(){
        randomNums = new Integer[amountOfNumbersNeeded];

        final Set<Integer> availableNumbers = new LinkedHashSet<>();
        while(availableNumbers.size() < amountOfNumbersNeeded){
            availableNumbers.add(randomNumGen.nextInt(topNumberBound));
        }

        availableNumbers.toArray(randomNums);
        LOG.info("Random Numbers:\t {}", Arrays.toString(randomNums).replace(",", "\t"));

        return randomNums;
    }

    public Float[] generateProbabilities(){
        probabilities = new Float[amountOfNumbersNeeded];

        final Float[] floats = new Float[amountOfNumbersNeeded];

        float sum = 0;
        for(int i= 0;  i< amountOfNumbersNeeded; i++){
            final float randomFloat = randomNumGen.nextFloat();
            floats[i] = randomFloat;
            sum += randomFloat;
        }

        //normalise to 1
        for(int i= 0;  i< amountOfNumbersNeeded; i++){
            probabilities[i] = round(floats[i] / sum, 3);
        }

        LOG.info("Probabilities:\t {}", Arrays.toString(probabilities).replace(",", "\t"));

        return probabilities;
    }

    /**
     Calls nextNum() defined number of times and returns results
     */
    public String nextNum(final int repetitions) {
        validateLoopArgument(repetitions);

        LOG.info("nextNum : Running in loop for [{}] times", repetitions);
        final Stopwatch stopwatch = Stopwatch.createStarted();

        for(int i = 0; i< repetitions; i++){
            nextNum();
        }

        stopwatch.stop();
        LOG.info("nextNum : Took {}ms", stopwatch.elapsed().toMillis());

        return getResults(repetitions);
    }

    private void validateLoopArgument(final int repetitions){
        LOG.debug("validateLoopArgument : repetitions=[{}]", repetitions);

        if(repetitions < 1)
            throw new IllegalArgumentException("repetitions must be greater than 0");
    }

    /**
     Returns one of the randomNums. When this method is called
     multiple times over a long period, it should return the
     numbers roughly with the initialized probabilities.
     */
    public int nextNum() {
        final float randomFloat = round(randomNumGen.nextFloat(), 2);

        final float[] cumProbSum = new float[amountOfNumbersNeeded];
        cumProbSum[0] = probabilities[0];

        for (int i = 1; i < amountOfNumbersNeeded; ++i)
            cumProbSum[i] = cumProbSum[i - 1] + probabilities[i];

        //Determine at which index randomFloat is in cumProbSum array and use that to return corresponding integer from randomNums
        final int index = findIndex(cumProbSum, randomFloat);

        final int randomNum = randomNums[index];
        counter.merge(randomNum, counter.getOrDefault(randomNum, 1), (k, v) -> v+1);

        return randomNum;
    }

    private String getResults(final int repetitions){
        final StringBuilder builder = new StringBuilder();
        builder.append("Results: \n");
        builder.append("Number\t Count\t Percentage\n");
        counter.forEach((k, v) -> builder.append(k).append("\t ").append(v).append("\t ").append(calculatePercentage(v, repetitions)).append("%\n"));

        return builder.toString().strip();
    }

    private double calculatePercentage(final double value, final double total){
        return new BigDecimal(value*100).divide(new BigDecimal(total), 1, RoundingMode.HALF_EVEN).doubleValue();
    }

    private float round(float number, int scale) {
        return new BigDecimal(number).setScale(scale, RoundingMode.HALF_EVEN).floatValue();
    }

    private int findIndex(final float[] cumFreqSum, final float random) {
        int i = 0;
        int endIndex = cumFreqSum.length-1;
        int mid;

        while (i < endIndex)
        {
            mid = (endIndex + i) / 2;
            if(random > cumFreqSum[mid])
                i = mid + 1;
            else
                endIndex = mid;
        }

        return cumFreqSum[i] >= random ? i : cumFreqSum.length - 1;
    }

    public Map<Integer, Integer> getCounter(){
        return counter;
    }
} 