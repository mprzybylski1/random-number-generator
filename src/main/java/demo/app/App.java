package demo.app;

import demo.config.ParameterLookup;
import demo.generator.RandomGen;
import demo.validator.InputValidator;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class App {

    private static final Logger LOG = LoggerFactory.getLogger(App.class);
    private static final InputValidator validator = new InputValidator();

    public static void main(final String[] args) throws ParseException {

        final Map<String, Integer> arguments = validator.validate(args);
        final int repetitions = arguments.get(ParameterLookup.REPETITIONS_PARAM_NAME);

        final RandomGen randomGen = new RandomGen(arguments.get(ParameterLookup.TOP_BOUND_PARAM_NAME), arguments.get(ParameterLookup.AMOUNT_NEEDED_PARAM_NAME));

        LOG.info(randomGen.nextNum(repetitions));
    }
}
