package demo.config;

public class StaticDataService {

    private static final int MIN_BOUND = 3;
    private static final int MAX_BOUND = 10;
    private static final int MIN_AMOUNT_NEEDED = 3;
    private static final int MAX_AMOUNT_NEEDED = 100;
    private static final int MIN_REP = 10;
    private static final int MAX_REP = 1_000_000;

    public static int getMinimumBoundLimit(){
        return MIN_BOUND;
    }

    public static int getMaximumBoundLimit(){
        return MAX_BOUND;
    }

    public static int getMinimumAmountNeededLimit(){
        return MIN_AMOUNT_NEEDED;
    }

    public static int getMaximumAmountNeededLimit(){
        return MAX_AMOUNT_NEEDED;
    }

    public static int getMinimumRepetitionsLimit(){
        return MIN_REP;
    }

    public static int getMaximumRepetitionsLimit(){
        return MAX_REP;
    }
}
