package demo.config;

public class ParameterLookup {

    public static final String TOP_BOUND_PARAM_NAME = "bound";
    public static final String AMOUNT_NEEDED_PARAM_NAME = "amount_needed";
    public static final String REPETITIONS_PARAM_NAME = "reps";

}
