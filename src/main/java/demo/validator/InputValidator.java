package demo.validator;

import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

import static demo.config.ParameterLookup.*;
import static demo.config.StaticDataService.*;
import static demo.validator.InputValidator.MsgType.MAX;
import static demo.validator.InputValidator.MsgType.MIN;

public class InputValidator {

    private static final Logger LOG = LoggerFactory.getLogger(InputValidator.class);

    protected enum MsgType {
        MIN("<argName> argument is too small. Minimum accepted value is: "),
        MAX("<argName> argument is too big. Maximum accepted value is: ");

        final String errorMsg;

        MsgType(final String errorMsg){
            this.errorMsg = errorMsg;
        }
    }

    private final Options options = new Options();
    private final CommandLineParser parser = new DefaultParser();

    public InputValidator(){
        options
                .addOption("bound", "", true, "Upper bound of the number range, from which numbers will be picked at random")
                .addOption("amount_needed", "", true, "The amount of numbers to be picked from the range")
                .addOption("reps", "", true, "Amount of repetitions nextNum method is called");
    }

    public Map<String, Integer> validate(final String[] cmdInput) throws IllegalArgumentException, ParseException {
        LOG.debug("Validating input arguments against predefined configuration");

        final CommandLine cmd = parser.parse(options, cmdInput);
        final Map<String, Integer> args = new HashMap<>();

        validateLimits(cmd, args::put, TOP_BOUND_PARAM_NAME, getMinimumBoundLimit(), getMaximumBoundLimit());
        validateLimits(cmd, args::put, AMOUNT_NEEDED_PARAM_NAME, getMinimumAmountNeededLimit(), getMaximumAmountNeededLimit());
        validateLimits(cmd, args::put, REPETITIONS_PARAM_NAME, getMinimumRepetitionsLimit(), getMaximumRepetitionsLimit());

        final StringBuilder builder = new StringBuilder();
        LOG.info("Using the following inputs:");
        args.forEach((k, v) -> builder.append(k).append("=[").append(v).append("], "));
        builder.setLength(builder.length()-2);

        LOG.info(builder.toString());

        return args;
    }

    private void validateLimits(final CommandLine cmd, final BiConsumer<String, Integer> consumer, final String argumentName, final int minLimit, final int maxLimit){
        final int argumentValue = parseOptionInt(cmd, argumentName);

        validateMinLimit(argumentName, argumentValue, minLimit);
        validateMaxLimit(argumentName, argumentValue, maxLimit);

        consumer.accept(argumentName, argumentValue);
    }

    private void validateMinLimit(final String argumentName, final int argumentValue, final int limit){
        validateArgument(argumentName, limit, MIN, (minLimit) -> argumentValue < minLimit);
    }

    private void validateMaxLimit(final String argumentName, final int argumentValue, final int limit){
        validateArgument(argumentName, limit, MAX, (maxLimit) -> argumentValue > maxLimit);
    }

    private int parseOptionInt(final CommandLine cmd, final String optionName){
        try{
            return Integer.parseInt(parseOption(cmd, optionName));
        }catch(NumberFormatException e){
            throw new IllegalArgumentException("Incorrect value for the following argument: " + optionName + ". Please provide integer value for this argument.", e);
        }
    }

    private String parseOption(final CommandLine cmd, final String optionName){
        final String optionValue = cmd.getOptionValue(optionName);

        if(optionValue == null)
            throw new IllegalArgumentException("Missing required value for argument [" + optionName + "] (" + options.getOption(optionName).getDescription() + ")");

        return optionValue;
    }

    private void validateArgument(final String argumentName, final int limit, final MsgType msgType, final Predicate<Integer> predicate){
        if(predicate.test(limit))
            throw new IllegalArgumentException(msgType.errorMsg.replace("<argName>", argumentName) + limit);
    }
}
